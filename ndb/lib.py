"""
The library powering this bot.
"""

import configparser
import contextlib
import functools
import os
import traceback
from typing import Union

import discord
from discord.ext import commands
from sqlalchemy import create_engine
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm import declarative_base, Session, sessionmaker

DeclarativeGuildBase = declarative_base(name="DeclarativeGuildBase")


class Cog(commands.Cog):
    __name__ = None

    def __init__(self, bot):
        self.bot: Bot = bot
        setattr(self.bot, self._get_name(), self)

    def _get_name(self):
        return self.__name__ if self.__name__ is not None else type(self).__name__.lower()


class BotDatabase:
    """SQLAlchemy Discord bot database."""

    def __init__(
            self,
            bot,
            sqlite_file: str = '.db',
            connection_string: str = None,
            declarative=DeclarativeGuildBase,
    ):
        self.bot = bot
        self.engine = create_engine(connection_string or f"sqlite:///{sqlite_file}")
        self.declarative = declarative
        self.declarative.metadata.create_all(self.engine)
        self.session_maker = sessionmaker(bind=self.engine)

    @contextlib.contextmanager
    def session(self) -> Session:
        sess = self.session_maker()
        try:
            yield sess
        except SQLAlchemyError:
            sess.rollback()
        except Exception:  # noqa
            traceback.print_exc()
        finally:
            sess.commit()


class DiscordConfigParser(configparser.ConfigParser):
    require_sections = ("permissions", "channels", "roles", "emojis", "guilds")

    def __init__(self, bot, *args, **kwargs):
        self.bot = bot
        super().__init__(*args, **kwargs)

    def optionxform(self, optionstr: str) -> str:
        return optionstr  # don't corrupt option names

    def get_permissions(self, adjective):
        return list(map(int, self.get("permissions", adjective).split(",")))

    def get_permission(self, adjective):
        """
        Return the rightmost permission ID from config[permissions][adjective].
        """
        return self.get_permissions(adjective).pop()

    def get_channel(
            self,
            adjective
    ) -> Union[discord.DMChannel, discord.TextChannel, discord.VoiceChannel]:
        return self.bot.get_channel(self.getint("channels", adjective))

    def get_role(self, adjective) -> discord.Role:
        return self.bot.main_guild.get_role(
            self.getint("roles", adjective)
        )

    def get_emoji(
            self,
            adjective
    ) -> Union[discord.Emoji, discord.PartialEmoji]:
        return self.bot.get_emoji(self.getint("emojis", adjective))

    def get_guild(
            self,
            adjective
    ) -> discord.Guild:
        return self.bot.get_guild(self.getint("guilds", adjective))


class Bot(commands.Bot):
    """A Discord bot with plugin, tasks and SQLAlchemy database support."""
    cog_classes = ()
    tasks_classes = ()
    config_string = None
    db_class = BotDatabase
    site = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._config = DiscordConfigParser(self)
        self._config.read_string(self.config_string)
        self._tasks = [task(bot=self) for task in self.tasks_classes]
        for cog in self.cog_classes:
            self.add_cog(cog(self))
        for task in self.tasks:
            task.loop.start()

    @functools.cached_property
    def db(self):
        return self.db_class(
            bot=self,
            connection_string=os.getenv('DB_CONN_STRING'),
        )

    @property
    def main_guild(self) -> discord.Guild:
        return self.config.get_guild("main")

    @property
    def config(self) -> DiscordConfigParser:
        return self._config

    @property
    def tasks(self) -> tuple:
        return tuple(self._tasks)

    @classmethod
    def configure(cls, **kwargs):
        for kwarg, opt in kwargs.items():
            setattr(cls, kwarg, opt)

    def __getattr__(self, item):
        raise AttributeError(f"{self.__class__.__name__!r} object has no attribute {item!r}")
