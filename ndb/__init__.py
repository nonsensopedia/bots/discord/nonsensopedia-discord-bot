from os.path import dirname, join, sep

top_level_path = dirname(__file__)
repo_path = dirname(top_level_path)
logs_path = join(top_level_path, "logs" + sep)

del dirname, join, sep
