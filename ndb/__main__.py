"""
The main Nonsensopedia Discord Bot program.
"""

import argparse
import os

from discord import Intents

from ndb.bot import Bot


def get_discord_token():
    """
    Returns the Discord token to use by the bot.
    """
    path = os.getenv('DISCORD_TOKEN_FILE')
    if path:
        return open(path, 'r').read().strip()
    return os.getenv("DISCORD_TOKEN") or args.token


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("--token", help="The Discord bot's token.")
    args = arg_parser.parse_args()
    bot = Bot("!", intents=Intents.all())
    bot.run(get_discord_token())
