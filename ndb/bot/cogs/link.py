"""
Plugin that reacts to Wikicode page and template references, by providing
links leading to them.
"""

import functools
import urllib.parse

import discord
from mwparserfromhell.nodes import Template, Wikilink
from mwparserfromhell.utils import parse_anything

from ndb.lib import Cog


class Link(Cog):
    """
    Establishes URLs of the user links sent to the guild with the bot's
    MediaWiki site.
    """

    @functools.cached_property
    def magic_words(self):
        self.bot.site.getmagicwords("")
        return {
            word.lower()
            for magic in getattr(self.bot.site, "_magicwords").values()
            for word in magic
        }

    def get_url(self, title: str):
        """
        Returns a (*title*, *url*) pair for a linked page.
        """
        return self.bot.site.base_url('/wiki/') + urllib.parse.quote(title)

    @Cog.listener()
    async def on_message(self, message):
        """
        Track Wikilinks from the message and respond with embedded
        URLs.
        """
        wikicode = parse_anything(message.clean_content.join("  "))
        queue = {}
        references = [
            *wikicode.filter_wikilinks(),
            *wikicode.filter_templates()
        ]
        for reference in references:
            if isinstance(reference, Wikilink):
                title = str(reference.title)
            else:
                title = str(reference.name)
                suffix = ""
                if ":" in title:
                    suffix = ":"
                cutting = title.split(":")[0].lower() + suffix
                if cutting in self.magic_words:
                    continue
            if isinstance(reference, Template):
                title = "Template:" + title
            url = self.get_url(str(title).strip())
            if title not in queue:  # don't mess up the order
                queue[title] = url

        description = "\n".join(
            [f"[{title}]({url})" for title, url in queue.items()]
        )
        quantity_suffix = ("", ("ów", "i")[
            str(len(queue))[-1] in "1234"
            and len(queue) not in range(11, 15)])[len(queue) > 1]
        embed = discord.Embed(
            colour=discord.Colour.random(seed=message.author.id),
            description=description
        ).set_footer(
            text=f"{len(queue)} link{quantity_suffix}"
        )

        if len(queue) < 50 and description:
            ctx = await self.bot.get_context(message)
            await ctx.channel.send(embed=embed)
