"""
Implements a Cog for authorizing main guild's members automatically and on demand.
"""

import asyncio
import contextlib
import datetime
import functools
import json
import logging
import os
import random
import traceback
from operator import truth
from os import path
from pathlib import Path

import discord
import mwoauth
import pytz
import pywikibot
from babel.dates import format_datetime, format_timedelta
from discord.errors import Forbidden
from discord.ext.commands import command, BucketType
from discord.ext.tasks import loop

from ndb import logs_path
from ndb.bot.tools import (
    has_roles, from_hint, inflect_timedelta, ping, user_signature, cooldown,
    skip_cooldown_if_admin
)
from ndb.bot.models import Account, ReactionRole, State
from ndb.lib import Cog

INVALID = object()
THROTTLED = object()
TZINFO = pytz.timezone("Europe/Warsaw")
ADMIN_SUPPORT_TEXT = (
    "Admini służą pomocą. Możesz ich @oznaczyć w razie jakichkolwiek "
    "problemów."
)
UNSAVED_TEXT = (
    "Konto użytkownika nie zostało zapisane. "
    "Usunięto status weryfikacji."
)
GREETINGS_TEXT = (
    "Powitajcie na pokładzie %(mention)s!",
    "Brawa dla %(mention)s, naszego %(which)s. członka!",
    "Bip-bup! %(mention)s właśnie wylądował na naszej planecie.",
    "A oto zjawił się %(mention)s – witamy!",
    "Miło nam cię tu widzieć, %(mention)s! Witamy!",
)


def cast_webhook_response(response) -> dict:
    """Cast JSON webhook response to dict."""
    response = str(response)
    if "{" in response and "}" in response:
        return json.loads(response[response.index("{"):response.rindex("}") + 1])
    raise ValueError(f"Couldn't parse webhook authentication response: {response!r}")


class OAuth:
    """
    OAuth transactions broker. Uses mwoauth.Handshaker.

    OAuth.callback is a callback URL template for callback URLs to be visited by the user as a
    result of redirect from a Nonsensopedia OAuth URL -- a 'redirect URL'.
    The 'Redirect URL' leads to Nonsensopedia OAuth login prompt, if the user did not log in
    before, and then redirects them to self.oauth.webhook URL
    with the 'oauth_verifier' and 'oauth_tokens' parameters.
    These parameters are added by the Nonsensopedia OAuth.
    Visiting OAuth.callback triggers a Discord webhook that sends back these
    parameters through a Discord message in JSON format in a special channel in a
    special guild (set in ndb/mwoauth.json, key 'guild').
    """
    callback = "https://ostrzyciel.pl/auth/a/{member_id}"

    def __init__(self, bot, data):
        self.bot = bot
        self.data = data
        self.site = self.bot.site
        self.handshaker = mwoauth.Handshaker(
            mw_uri=self.site.base_url(""),
            consumer_token=mwoauth.ConsumerToken(
                key=self.data["key"],
                secret=self.data["secret"],
            ),
            user_agent="NonsensopediaDiscordBot, Python 3",
        )

    @staticmethod
    def load_data():
        file = os.getenv('MWOAUTH_FILE') \
            or Path(path.dirname(__file__)).parent.parent.joinpath("mwoauth.json")
        data = json.loads(open(file, "r").read())
        tokens = {
            "key": str(data.get("key") or ""),
            "secret": str(data.get("secret") or ""),
            "webhook": int(data.get("webhook") or 0),
            "guild": int(data.get("guild") or 0),
        }
        return tokens

    def initiate(self, member_id: int):
        """Initiate the handshaker to produce a redirect URL and request token."""
        return self.handshaker.initiate(callback=self.callback.format(member_id=member_id))

    def complete_and_identify(
            self,
            state: State,
            response_qs: str,
    ):
        """Complete the OAuth process and, finally, identify the user."""
        request_token = mwoauth.RequestToken(*state.request_token.split(","))
        try:
            access_token = self.handshaker.complete(request_token, response_qs)
            user_info = self.handshaker.identify(access_token)
        except mwoauth.OAuthException:
            access_token = INVALID
            user_info = INVALID
            traceback.print_exc()
        except TypeError:
            access_token = THROTTLED
            user_info = THROTTLED
        return access_token, user_info


class Auth(Cog):
    """A Cog for authentications."""
    # pylint: ignore=broad-except

    __name__ = "auth"

    # Determines for how long a redirect URL is valid after it is produced.
    auth_url_expiry = datetime.timedelta(minutes=30)

    # Determines for how long an authentication state is valid after it is produced.
    # Once it is expired, the member bound to that state gets kicked from the guild.
    auth_state_expiry = datetime.timedelta(hours=24)

    # Determines how much time needs to be left until the user's authentication state
    # is expired to invoke next initialize() with remind=True option.
    time_left_to_remind = datetime.timedelta(hours=18)  # activates after 6 hours

    # 'accounts_busy', 'init_busy' and 'remind_busy' sets are used in order to guarantee that
    # the underlying parts of self.add_access(), self.remove_access() and self.initialize() and
    # self.initialize(..., remind=True) will execute without disturbances
    # caused by their asynchronicity. It also allows to limit of queries to the database.
    accounts_busy = set()
    init_busy = set()
    remind_busy = set()

    # Logger
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    logger_file_handler = logging.FileHandler(logs_path + __name__ + ".log")
    logger_file_handler.setLevel(logging.DEBUG)
    logger.addHandler(logger_file_handler)

    @functools.cached_property
    def oauth(self) -> OAuth:
        """OAuth transactions broker, computed once per instance."""
        return OAuth(self.bot, OAuth.load_data())

    @property
    def auth_channel(self) -> discord.TextChannel:
        """Channel to initialize and complete authentications in, set in ndb/config.ini"""
        return self.bot.config.get_channel("auth")

    @property
    def auth_emoji(self) -> discord.PartialEmoji:
        """
        Emoji allowing the user to confirm their willing to authenticate,
        set in ndb/config.ini.
        """
        return self.bot.config.get_emoji("auth")

    @property
    def pending_authentication_role(self) -> discord.Role:
        """
        A role that marks users who confirmed their willing to authenticate
        and received a redirect URL. Set in ndb/config.ini.
        """
        return self.bot.config.get_role("pending_authentication")

    @property
    def authenticated_role(self) -> discord.Role:
        """A role that marks users who are authenticated, set in ndb/config.ini."""
        return self.bot.config.get_role("authenticated")

    async def initiate_oauth(self, member_id: int):
        """
        Manages to initiate OAuth handshaker and maintain contact
        with the authenticated user during this time.
        """

        with self.bot.db.session() as sess:
            prestate = sess.get(State, member_id)
            member = self.bot.main_guild.get_member(member_id)
            if prestate is None:
                # Practically impossible case.
                # As self.initialize() registers new State object in the database,
                # achieving the following snippet would mean that the state was
                # deleted from the database before the user triggered the reaction role
                # that invokes this method.

                # self.oauth.initiate() initiates a handshaker that fetches the 'redirect URL'
                # for the user (more information on 'redirect URL' in the comment blocks below).
                redirect_url, request_token = self.oauth.initiate(member_id)
                state = State(
                    auth_expires_at=datetime.datetime.now() + self.auth_state_expiry,
                    guild_id=self.bot.main_guild.id,
                    member_id=member_id,
                    url_expires_at=datetime.datetime.now() + self.auth_url_expiry,
                    redirect_url=redirect_url,
                    request_token=",".join(request_token),
                )
            else:
                state = prestate
                if state.done:
                    # User (somehow?) triggered the old reaction role that invokes this method,
                    # but they are already authenticated.
                    # Revert added roles.
                    # Note 1: self.add_access() is not used as it's verbose.
                    # Note 2: there is no check of account presence.
                    await member.remove_roles(self.pending_authentication_role)
                    await member.add_roles(self.authenticated_role)
                    return

                if state.url_expires_at is not None:
                    if state.url_expires_at > datetime.datetime.now():
                        # There is a redirect URL present and not expired already.
                        # Don't continue.
                        return

                redirect_url, request_token = self.oauth.initiate(member_id)

                # State isn't done, so the user is unauthenticated and is willing to authenticate.
                url_expires_at = datetime.datetime.now() + self.auth_url_expiry
                if url_expires_at > state.auth_expires_at:
                    url_expires_at = url_expires_at + (state.auth_expires_at - url_expires_at)
                state.url_expires_at = url_expires_at
                state.redirect_url = redirect_url
                state.request_token = ",".join(request_token)

            # Compute the time left for authenticating via this newly produced 'redirect URL'.
            time_left = format_timedelta(
                state.url_expires_at - datetime.datetime.now(), locale="pl_PL"
            )

            # Share with the user the redirect URL to be visited by them.
            try:
                # Try to share with the user the redirect URL to be visited by them.
                await member.send(embed=discord.Embed(
                    colour=discord.Colour.random(seed=member.id),
                    description=f"Twój link weryfikacyjny:\n{redirect_url}\n"
                                "Pamiętaj, aby nikomu go nie udostępniać."
                ).set_footer(
                    text=f"Uwaga! Wygaśnie za {inflect_timedelta(time_left)}.")
                )
            except discord.Forbidden:
                # The Forbidden exception here usually means that the user doesn't accept
                # messages coming from users with mutual guilds.
                # TODO: are there any more cases when this error may be raised?
                await self.auth_channel.send(embed=discord.Embed(
                    colour=discord.Colour.random(seed=member.id),
                    description=(
                        f"{member.mention}, wystąpił błąd podczas wysyłania "
                        f"Tobie wiadomości prywatnej. Czy odbierasz wiadomości "
                        f"od członków serwerów, na których jesteś?"
                    )
                ).set_footer(
                    text=(
                        "Możesz tymczasowo włączyć zezwolenie na "
                        "wiadomości prywatne od członków serwerów. "
                        "Następnie ponownie użyć komendy !weryfikuj."
                    ))
                )
                state.url_expires_at = None  # bring back to the state where it was not issued
                state.redirect_url = ""
                state.request_token = ""
                sess.commit()
            else:
                # Notify the user in the authentications channel that they were DM-d.
                # Commit the state into the database, as there was no exception on the way.
                if prestate is None:
                    sess.commit()
                await self.auth_channel.send(embed=discord.Embed(
                    colour=discord.Colour.random(seed=member.id),
                    description=(
                        f"{member.mention}, wysłaliśmy ci link weryfikacyjny "
                        "w wiadomości prywatnej. Pamiętaj, aby nikomu go "
                        "nie udostępniać."
                    )
                ))

    async def complete_oauth(
            self,
            sess,
            state: State,
            oauth_verifier: str,
            oauth_token: str,
    ):
        """
        Manages to complete the whole authentication process.

        The final decision 'what to do' with the authenticated member always depends on the
        self.submit_member() method, invoked at the end of self.complete_auth().
        """
        guild = self.bot.get_guild(state.guild_id)
        member = guild.get_member(state.member_id)
        expired = False

        # This user is already authenticated, don't continue working on their authentication.
        if state.done:
            return

        # When State.url_expires_at is None, it means there was no redirect URL produced during
        # thw authentication state life.
        if state.url_expires_at is not None:
            if state.url_expires_at <= datetime.datetime.now():
                # The newest redirect URL expired, however try to verify using it.
                expired = True

        # Reproduce 'response query string', which are the parameters appended to the callback URL.
        response_qs = f"{oauth_verifier=!s}&{oauth_token=!s}"

        # Complete the authentication and fetch the account data.
        result = access_token, data = self.oauth.complete_and_identify(state, response_qs)

        if INVALID in result:
            # Something went wrong.
            if expired:
                # Seems we can't authenticate using this URL, authenticate again after expired.
                await self.initialize(sess, member, after_expired=True)
            return

        if THROTTLED in result:
            # Redirect URL was probably visited twice or more in too short time interval.
            # This caused that the access token was throttled.
            await self.auth_channel.send(embed=discord.Embed(
                colour=discord.Colour.random(seed=member.id),
                description=(
                    ":no_entry: Ups! Wystąpił błąd. Prosimy spróbować jeszcze raz, "
                    f"{member.mention}."
                )
            ))
            state.url_expires_at = None  # bring back to the state where it was not issued
            state.redirect_url = ""
            state.request_token = ""
            sess.commit()
            await self.initialize(sess, member)
            return

        user = pywikibot.User(self.bot.site, "User:" + data["username"])
        # Enrich the account data with account properties from the site.
        data = {**data, **user.getprops(force=True)}

        # **kwargs for Account.__init__
        account_schema = {"mw_username": data["username"]}
        if "userid" in data:
            account_schema["mw_userid"] = data["userid"]
        if "gender" in data:
            account_schema["mw_gender"] = data["gender"]

        # Construct an account to be registered.
        account = Account(**account_schema)
        account.member_id = state.member_id

        signature = user_signature(self.bot.site, account.mw_username)

        # Check if there is already a registered owner of the processed account.
        # In that case we don't allow the authentication to complete,
        # as only one member can be bound with an account and vice versa.
        preowned = sess.query(Account).filter_by(mw_userid=account.mw_userid).one_or_none()
        if preowned is not None:
            owner = self.bot.main_guild.get_member(preowned.member_id)
            await member.send(embed=discord.Embed(
                colour=discord.Colour.random(seed=member.id),
                description=(
                    f":no_entry: Konto {signature} "
                    "jest już zweryfikowane w naszej bazie i należy "
                    f"do {owner.mention}. Możesz poprosić "
                    "[Ostrzyciela](https://nonsa.pl/wiki/User:Ostrzyciel) "
                    "lub [Stima](https://nonsa.pl/wiki/User:Stim) o "
                    "anulowanie tej weryfikacji i zweryfikować się na "
                    "nowo."
                )
            ))
            await self.auth_channel.send(
                embed=discord.Embed(
                    colour=discord.Colour.random(seed=member.id),
                    description=(
                        f":no_entry: Nie zweryfikowano {member.mention}: "
                        f"konto {signature} "
                        f"już należy do {owner.mention}."
                    ))
            )
            return

        state.access_token = ",".join(access_token)
        state.done = True
        sess.add(account)

        with contextlib.suppress(Forbidden):
            await member.send(
                embed=discord.Embed(
                    colour=discord.Colour.random(seed=member.id),
                    description=f":white_check_mark: {signature}"
                )
            )
        await self.submit_member(sess, state, member, account, data)

    async def submit_member(self, sess, state, member, account: Account, account_data: dict):
        """Submits a member to complete the whole authentication process."""
        if await self.confirm_member(member, account_data):  # verbose
            await self.add_access(member, account)
        else:
            await self.remove_access(member)
            await self.expire(sess, state, member)

    async def add_access(self, member: discord.Member, account: Account):
        """
        Remove the member's 'access' to main channels of the guild.
        A member whose 'access' was removed can only send messages in the authentications
        channel (self.auth_channel) and read the welcome channel.

        :param member: Member to add the guild access to.
        :param account: Account of the member.
        """
        if member.id in self.accounts_busy:
            return
        self.accounts_busy.add(member.id)
        signature = user_signature(self.bot.site, account.mw_username)
        await member.remove_roles(self.pending_authentication_role)
        await member.add_roles(self.authenticated_role)
        await self.auth_channel.send(
            embed=discord.Embed(
                colour=discord.Colour.random(seed=member.id),
                description=(
                    f":white_check_mark: Użytkownik {member.mention} "
                    "uzyskał dostęp do serwera."
                    "\nKonto na Nonsensopedii: "
                    f"{signature}"
                ))
        )
        await self.greet(member, account)
        self.accounts_busy.discard(member.id)

    async def remove_access(self, member):
        """
        Remove the member's 'access' to main channels of the guild.
        A member whose 'access' was removed can only send messages in the authentications
        channel (self.auth_channel) and read the welcome channel.

        :param member: Member to remove the guild access to.
        """
        if member.id in self.accounts_busy:
            return
        self.accounts_busy.add(member.id)
        await member.remove_roles(self.authenticated_role)
        self.accounts_busy.discard(member.id)

    async def initialize(
            self,
            sess,
            member: discord.Member,
            after_expired=False,
            remind=False,
    ):
        """
        Initialize the whole authentication process.
        Accepts three special modes that determine the response.
        - when both 'after_expired' and 'remind' are False, the basic initialization happens.
        - 'after_expired' parameter, when set to True, will point out 'whoops, the verification
          link expired',
        - 'remind' parameter, when set to True, requires the member's state to exist in the
          database. It reminds the member to be aware of authentication state expiry that causes
          in kick.
        """

        if remind and after_expired:
            raise TypeError(f"inconsistence: {after_expired=} and {remind=}")

        if member.id in self.init_busy:
            return

        self.init_busy.add(member.id)

        if isinstance(member, discord.User):
            if self.bot.main_guild not in member.mutual_guilds:
                return
            member = self.bot.main_guild.get_member(member.id)

        if after_expired:
            # The user's newest redirect URL is expired while initialize() is invoked.
            await member.remove_roles(self.authenticated_role)
            await ping(member, self.auth_channel)
            message = await self.auth_channel.send(
                embed=discord.Embed(
                    colour=discord.Colour.random(seed=member.id),
                    description=(
                        f"Aj {member.mention}, dotychczasowy link wygasł. "
                        "\n\n"
                        "**Aby otrzymać kolejny link weryfikacyjny, zostaw "
                        f"{self.auth_emoji} w reakcji pod tą "
                        f"wiadomością. "
                        "**"
                    )
                ).set_footer(text=ADMIN_SUPPORT_TEXT)
            )
        elif remind:
            state = sess.get(State, member.id)

            if state is None:
                raise ValueError(
                    f"state is not registered and {remind=} is set"
                )

            if state.reminded:
                raise ValueError("member already reminded")

            if state.member_id in self.remind_busy:
                await self.initialize(sess, member)

            self.remind_busy.add(state.member_id)

            time_left = inflect_timedelta(format_timedelta(
                state.auth_expires_at - datetime.datetime.now(),
                locale="pl_PL"
            ))

            await member.remove_roles(self.authenticated_role)
            await ping(member, self.auth_channel)
            message = await self.auth_channel.send(
                embed=discord.Embed(
                    colour=discord.Colour.random(seed=member.id),
                    description=(
                        f"Hej {member.mention}, "
                        "nadal się nie zweryfikowałeś! :slight_smile:"
                        "\nZ tego serwera można korzystać tylko po "
                        "potwierdzeniu swojego konta na Nonsensopedii."
                        "\nMożesz je założyć tutaj: "
                        "https://nonsa.pl/wiki/Specjalna:Utwórz_konto "
                        "\nAutomatyczne wyrzucenie "
                        f"nastąpi za {inflect_timedelta(time_left)}. "
                        "\n\n**Aby zweryfikować swoje konto, zostaw "
                        f"{self.auth_emoji} w reakcji pod tą wiadomością."
                        "**"
                    )
                ).set_footer(text=ADMIN_SUPPORT_TEXT)
            )
            state.reminded = True
            sess.commit()
        else:
            await member.remove_roles(self.authenticated_role)
            await ping(member, self.auth_channel)
            message = await self.auth_channel.send(
                embed=discord.Embed(
                    colour=discord.Colour.random(seed=member.id),
                    description=(
                        f"Witaj, {member.mention}! :slight_smile:\n"
                        "Żeby korzystać z tego serwera, potrzebujesz "
                        "konta na Nonsensopedii. Jeśli go nie masz, "
                        "załóż je: "
                        "https://nonsa.pl/wiki/Specjalna:Utwórz_konto"
                        "\nJeśli dopiero założyłeś konto na Discordzie, "
                        "będziesz musiał poczekać 5 minut."
                        "\n\n**Aby zweryfikować swoje konto, zostaw "
                        f"{self.auth_emoji} w reakcji pod tą wiadomością."
                        "**\n\n"
                    )
                ).set_footer(text=ADMIN_SUPPORT_TEXT).set_thumbnail(
                    url=(
                        "https://nonsa.pl/images/thumb/f/f0/Wiki.svg/"
                        "233px-Wiki.svg.png"
                    )
                )

            )

        if sess.get(State, member.id) is None:
            state = State(
                auth_expires_at=datetime.datetime.now() + self.auth_state_expiry,
                member_id=member.id,
                guild_id=self.bot.main_guild.id,
                redirect_url="",
                request_token=""
            )
            sess.add(state)
            sess.commit()

        await message.add_reaction(self.auth_emoji)
        reaction_role = ReactionRole(
            member_id=member.id,
            emoji_id=self.auth_emoji.id,
            guild_id=member.guild.id,
            channel_id=self.auth_channel.id,
            role_id=self.pending_authentication_role.id,
            message_id=message.id,
            temporary=True,
            callback=f"self.bot.{self._get_name()}.initiate_oauth({member.id})",
        )
        reaction_role.id = id(reaction_role)
        if not sess.query(ReactionRole).filter_by(
            member_id=reaction_role.member_id,
            emoji_id=reaction_role.emoji_id,
            guild_id=reaction_role.guild_id,
            channel_id=reaction_role.channel_id,
            message_id=reaction_role.message_id
        ).all():
            sess.add(reaction_role)
        sess.commit()
        self.init_busy.discard(member.id)

    async def expire(
            self, sess, state: State, member: discord.Member
    ):
        """Expires a member authentication state and their account, if it's present."""
        account = sess.get(Account, member.id)
        sess.delete(state)
        if account is not None:
            sess.delete(account)
        # Don't wait; commit straight away to avoid integrity errors
        # (session may be nested).
        sess.commit()
        # Purge self.remind_busy cache.
        self.remind_busy.discard(state.member_id)

    async def renew(
            self, sess, state: State
    ):
        """Renews the authentication process for a member."""
        member = self.bot.main_guild.get_member(state.member_id)
        await self.expire(sess, state, member)
        await self.initialize(sess, member)

    async def greet(self, member, account):
        channel = self.bot.config.get_channel("offtopic")
        await channel.send(
            random.choice(GREETINGS_TEXT) % dict(
                mention=member.mention,
                which=self.bot.main_guild.member_count
            ),
            embed=await self.create_whois_embed(account, member)
        )

    async def confirm_member(
            self,
            member: discord.Member,
            account_data,
            verbose=True,
    ):
        passing = True
        signature = user_signature(
            self.bot.site, account_data["name"]
        )
        admins = self.bot.config.get_channel("admins")
        if account_data.get("blockid") is not None:
            if account_data["blockexpiry"] not in ("infinite", "indefinite"):
                block_expiry = pywikibot.Timestamp.fromISOformat(account_data["blockexpiry"])
                timedelta = format_timedelta(
                    block_expiry - account_data["blockedtimestamp"],
                    locale="pl_PL"
                )
                time_left = format_timedelta(
                    block_expiry - datetime.datetime.utcnow(),
                    locale="pl_PL"
                )
                if verbose:
                    await admins.send(embed=discord.Embed(
                        colour=discord.Colour.random(seed=member.id),
                        description=(
                            "Na kanale weryfikacyjnym pojawił się "
                            f"{signature} ({member.mention}), "
                            f"zablokowany na {timedelta}. "
                            f"Do końca blokady jeszcze {time_left}."
                        )))
                passing = True
            elif account_data["blockexpiry"] in ("indefinite", "infinite"):
                if verbose:
                    await admins.send(embed=discord.Embed(
                        colour=discord.Colour.random(seed=member.id),
                        description=(
                            "Na kanale weryfikacyjnym pojawił się "
                            f"{signature} ({member.mention}), "
                            "zablokowany na zawsze."
                        )))

                    await self.auth_channel.send(
                        embed=discord.Embed(
                            colour=discord.Colour.random(seed=member.id),
                            description=(
                                f":no_entry: Błąd weryfikacji {member.mention}: "
                                f"konto {signature} zostało zablokowane na zawsze."
                            )
                        ).set_footer(text=UNSAVED_TEXT)
                    )
                passing = False
            else:
                if verbose:
                    await admins.send(embed=discord.Embed(
                        colour=discord.Colour.random(seed=member.id),
                        description=(
                            "Na kanale weryfikacyjnym pojawił się "
                            "zablokowany "
                            f"{member.mention} (szczegóły niedostępne)."
                        )))
                passing = True
        if "bot" in account_data.get("groups", []):
            if verbose:
                await self.auth_channel.send(embed=discord.Embed(
                    colour=discord.Colour.random(seed=member.id),
                    description=(
                        f":no_entry: Błąd weryfikacji {member.mention}: "
                        f"konto {signature} należy do bota."
                    )
                ).set_footer(text=UNSAVED_TEXT))
                await admins.send(
                    embed=discord.Embed(
                        colour=discord.Colour.random(seed=member.id),
                        description=(
                            "Na kanale weryfikacyjnym pojawił się "
                            f"{signature} ({member.mention}), "
                            "który próbował zweryfikować się kontem bota."
                        )
                    )
                )
                passing = False
        return passing

    async def kick_member_after_expiry(self, sess, state):
        member = self.bot.main_guild.get_member(state.member_id)
        if member is None:
            # State is present and expired, but the member left the guild.
            return
        if datetime.datetime.now() < state.auth_expires_at:
            # Check again if the invoke context is correct.
            return
        admins = self.bot.config.get_channel("admins")
        await admins.send(embed=discord.Embed(
            colour=discord.Colour.random(seed=member.id),
            description=f"Wyrzucam `{member}` (czas na weryfikację minął)."
        ))
        try:
            # Try to kick the member.
            await member.kick(reason="Czas na weryfikację minął")
        except Forbidden:
            # We've no rights to perform this kick, but the admins were notified, and may know
            # the background on this, so we may suppress it.
            pass
        except Exception:  # noqa
            # Other exception occured while performing, elaborate on it.
            traceback.print_exc()
        finally:
            # Note:
            #   This deletes the user's account (if present) and authentication state from
            #   the database.
            await self.expire(sess, state, member)

    @Cog.listener()
    async def on_message(self, message: discord.Message):
        # This may be message from the webhook, process it.
        # TODO:
        #  change the webhook app to directly invoke the self.process_webhook_response()
        #  command
        if message.webhook_id != self.oauth.data["webhook"]:
            # Don't pursue working on it, it's not the webhook we look for.
            return
        ctx = await self.bot.get_context(message)
        await self.process_webhook_response(ctx)

    @command("webhook")
    @has_roles("backend_admin", allow_webhook=OAuth.load_data().get("webhook"))
    async def process_webhook_response(self, ctx):
        """Processes a webhook response."""
        try:
            # 1. Cast it to a dict.
            response = cast_webhook_response(ctx.message.clean_content)
        except ValueError:
            await ctx.reply(embed=discord.Embed(
                colour=discord.Colour.random(seed=ctx.message.author.id),
                description=":no_entry: Format webhooka jest nieprawidłowy."
            ))
        else:
            # 2. Dispatch to the handler.
            await self.on_webhook_response(response)

    async def on_webhook_response(self, response: dict):
        member = self.bot.main_guild.get_member(response.pop("discord_id"))
        if member is None:
            # This might be a webhook for the same application working simultaneously.
            return
        with self.bot.db.session() as sess:
            state = sess.get(State, member.id)
            if state is None:
                # We ignore this response, as the authentication state was
                # not registered in the bot database.
                return
            if not state.done:
                # Authentication was not completed. Complete it now.
                await self.complete_oauth(sess, state=state, **response)
        # Otherwise, the authentication was already completed and there's
        # no need to handle that webhook response.
        self.init_busy.discard(member.id)

    @Cog.listener()
    async def on_member_join(self, member: discord.Member):
        # Check if this join is significant.
        if member.guild.id != self.bot.main_guild.id:
            return
        # The main guild got a new member.
        with self.bot.db.session() as sess:
            state: State = sess.get(State, member.id)

            if state is None:
                # State of the new member is unregistered, register them.
                await self.initialize(sess, member)
                return

            # State of the new member is registered in the database.
            # This part is unreachable in case the member was kicked because of
            # their authentication process expiry.
            # A rejoin resets the State.auth_expires_at value.
            state.auth_expires_at = datetime.datetime.now() + self.auth_state_expiry
            sess.commit()

            account = sess.get(Account, member.id)

            if account is None:  # equivalent to 'if not state.done:'
                # Account is unregistered -> the authentication process is ongoing.
                if state.url_expires_at is not None:
                    # The redirect URL was issued before.
                    # In case that URL expired, we ignore it, and produce a new one.
                    # 'after_expired' control variable just switches the mode.
                    after_expired = state.url_expires_at <= datetime.datetime.now()
                    await self.initialize(sess, member, after_expired=after_expired)
                else:
                    # There was no redirect URL issued before.
                    # Initialize in the standard way.
                    await self.initialize(sess, member)
            else:
                # Account is registered -> the authentication process is finished.
                # Give them back their member access.
                account_data = pywikibot.User(
                    self.bot.site,
                    "User:" + account.mw_username
                ).getprops(True)
                await self.submit_member(sess, state, member, account, account_data)

    @cooldown(2, 120.0, BucketType.user, skip_cooldown_if_admin)
    @command(name="weryfikuj", aliases=["autoryzuj", "weryfikacja"])
    async def authenticate(self, ctx, user_hint: str = ""):
        """The underlying Auth cog command."""
        member = from_hint(user_hint, ctx, self.bot.main_guild)
        if member is None:
            return

        with self.bot.db.session() as sess:
            state = sess.get(State, member.id)

            if state is None:
                # Authentication state of this member is unregistered,
                # initialize a new authentication in the standard way.
                await self.initialize(sess, member)
                return

            if state.done:
                # The user's authentication state is registered and marked as done
                # in the database -> account is registered.
                account = sess.get(Account, state.member_id)

                if account is None:
                    # Practically unreachable.
                    # State is marked as done only when an account was also registered in
                    # the database.
                    await self.renew(sess, state)
                    return

                signature = user_signature(self.bot.site, account.mw_username)
                await ping(member, ctx.channel)
                await ctx.channel.send(embed=discord.Embed(
                    colour=discord.Colour.random(seed=member.id),
                    description=(
                        f":no_entry: {member.mention}, "
                        "Twoje konto jest "
                        f"już zweryfikowane: {signature}. "
                        "Możesz poprosić któregoś z technicznych ([Ostrzyciel]("
                        "https://nonsa.pl/wiki/User:Ostrzyciel) "
                        "lub "
                        "[Stim](https://nonsa.pl/wiki/User:Stim)) o "
                        "anulowanie tej weryfikacji, aby zweryfikować "
                        "się na nowo."
                    )
                ))
                return

            # The user's to be verified state registered and not marked as done in the database.
            now = datetime.datetime.now()

            was_issued = False
            if state.url_expires_at is not None:
                was_issued = True
                if state.url_expires_at > now:
                    time_left = inflect_timedelta(format_timedelta(
                        now - state.url_expires_at, locale="pl_PL"
                    ))
                    # The newest redirect URL has not expired.
                    await self.auth_channel.send(embed=discord.Embed(
                        colour=discord.Colour.random(seed=member.id),
                        description=(
                            f"{member.mention}, już wysłaliśmy ci link weryfikacyjny "
                            "w wiadomości prywatnej :slight_smile:."
                        )
                    ).set_footer(text=f"Wygaśnie za {time_left}."))
                    return
                # Otherwise the newest redirect URL has expired or was never issued, continue.

            if state.auth_expires_at <= now:
                # The authentication process has expired.
                # Note: self.kick_expired() deletes the state from the database.
                await self.kick_member_after_expiry(sess, state)
            else:
                # The authentication process has not expired.
                if (
                        (state.auth_expires_at
                         - datetime.datetime.now())
                        <= self.time_left_to_remind
                ):
                    # Time left until the authentication ends is equal or less than time
                    # required to be left until then to invoke the self.initialize() method with
                    # remind set to True.

                    if not state.reminded:
                        # The member was not reminded to be aware of the approaching expiry kick.
                        # Initialize an authentication that will tell the user how much time is
                        # left.
                        await self.initialize(sess, member, remind=True)
                    else:
                        # The member was reminded to be aware of the approaching expiry kick.
                        # Initialize a standard authentication.
                        await self.initialize(sess, member)
                else:
                    # The newest authentication redirect URL is expired or was never issued,
                    # because if that URL is expired and was issued, authenticate() method
                    # execution stops.
                    await self.initialize(sess, member, after_expired=was_issued)

    async def _expire_account_impl(self, ctx, member, sess):
        state = sess.get(State, member.id)
        if state is None:
            return
        await self.expire(sess, state, member)
        await ctx.send(
            embed=discord.Embed(
                colour=discord.Colour.random(seed=member.id),
                description=(
                    ":white_check_mark: "
                    f"{member.mention}, pomyślnie usunięto "
                    "z naszej bazy przypisane "
                    "Tobie konto i status weryfikacji."
                )
            )
        )

    @command(name="unieważnij", aliases=["archiwizuj"])
    @has_roles("backend_admin")
    async def expire_account(self, ctx, user_hint: str = ""):
        """Expire an account's authentication state and possibly registered account."""
        with self.bot.db.session() as sess:
            member = from_hint(user_hint, ctx, self.bot.main_guild)
            if member is None and not user_hint:
                return
            await self._expire_account_impl(ctx, member, sess)

    @command(name="reweryfikuj", aliases=["reautoryzuj"])
    @has_roles("backend_admin")
    async def reauthenticate_account(self, ctx, user_hint: str = ""):
        """
        Expire an account's authentication state and possibly registered account in the
        database and initialize a new authentication process for it.
        """
        member = from_hint(user_hint, ctx, self.bot.main_guild)
        if member is None:
            return
        with self.bot.db.session() as sess:
            state = sess.get(State, member.id)
            if state is not None:
                await self._expire_account_impl(ctx, member, sess)
            await self.initialize(sess, member)

    @cooldown(2, 120.0, BucketType.user, skip_cooldown_if_admin)
    @command(name="whois", aliases=["konto"])
    @has_roles("authenticated")
    async def check_account(self, ctx, *user_hint):
        """Check an account, fetching its information from Nonsensopedia."""
        member = from_hint(user_hint, ctx, self.bot.main_guild)
        if member is None:
            return
        with self.bot.db.session() as sess:
            state = sess.get(State, member.id)
            account = sess.get(Account, member.id)
        if state is None:
            await ctx.reply(
                embed=discord.Embed(
                    colour=discord.Colour.random(seed=member.id),
                    description=(
                        f":no_entry: Użytkownik {member.mention} nie jest "
                        "zweryfikowany."
                    )
                )
            )
        elif not state.done:
            time_left = inflect_timedelta(format_timedelta(
                state.auth_expires_at - datetime.datetime.now(),
                locale="pl_PL"
            ))
            time_left = time_left if time_left != "dzień" else "24 godziny"
            await ctx.reply(
                embed=discord.Embed(
                    colour=discord.Colour.random(seed=member.id),
                    description=(
                        f"Użytkownik {member.mention} nie dokończył "
                        f"weryfikacji."
                    )
                ).set_footer(text=f"Weryfikacja wygasa za {time_left}.")
            )
        else:
            embed = await self.create_whois_embed(account, member)
            await ctx.send(embed=embed)

    async def create_whois_embed(
            self,
            account: Account,
            member: discord.Member
    ) -> discord.Embed:
        """Creates a !whois embed."""
        signature = user_signature(self.bot.site, account.mw_username)
        user_page = pywikibot.User(self.bot.site, "User:" + account.mw_username)
        user_data = user_page.getprops(True)
        discord_registration = format_datetime(
            TZINFO.fromutc(member.created_at.replace(tzinfo=TZINFO)),
            locale="pl_PL"
        )
        mention = f"Konto na Discordzie: {member.mention} (ur. {discord_registration})"
        registration = user_data.get("registration")
        on_leave, contribs, blocked = ("",) * 3
        templates = dict(user_page.templatesWithParams())
        leave_template = pywikibot.Page(self.bot.site, "Szablon:Urlop")

        if leave_template in templates:
            # The user is on leave.
            params = templates[leave_template]
            if params:
                on_leave = f"**Na urlopie**. Wróci {' '.join(params)}."
            else:
                on_leave = '**Na urlopie**.'

        if registration is not None:
            # Add the user registration date.
            registration = (pywikibot.Timestamp.fromISOformat(registration))
            delta = inflect_timedelta(format_timedelta(
                datetime.datetime.utcnow() - registration,
                locale="pl_PL"
            ))
            registered_when = format_datetime(
                TZINFO.fromutc(registration.replace(tzinfo=TZINFO)),
                locale="pl_PL"
            ) + " (" + delta + " temu)"
            registration = (
                f"Założył{('', 'a')[account.mw_gender == 'female']} konto: "
                f"{registered_when}"
            )

        if "editcount" in user_data:
            # There is an edit count, so get the last edit information and append to the embed.
            contribs = 'Ostatnia edycja: '
            last_edit = user_page.last_edit
            if last_edit is not None:
                last_edit_timedelta = inflect_timedelta(format_timedelta(
                    datetime.datetime.utcnow() - last_edit[2],
                    locale="pl_PL"
                ))
                contribs += f"{last_edit_timedelta} temu"
            else:
                contribs = ""

        # The user groups.
        groups = "Grupy: " + ", ".join(filter("*".__ne__, user_data["groups"]))

        if user_data.get("blockedtimestamp") is not None:
            # The user is blocked.
            with self.bot.db.session() as sess:
                blocked_by_account = (
                    sess.query(Account).filter_by(
                        mw_userid=user_data["blockedbyid"]
                    ).one_or_none()
                )
            if blocked_by_account:
                blocked_by = self.bot.main_guild.get_member(blocked_by_account.member_id).mention
            else:
                blocked_by = user_data["blockedby"]
            block_expiry = (
                format_datetime(
                    TZINFO.fromutc(
                        pywikibot.Timestamp.fromISOformat(
                            user_data["blockexpiry"]
                        ).replace(tzinfo=TZINFO)
                    ),
                    locale="pl_PL"
                )
                if user_data["blockexpiry"] != "infinite"
                else "dnia sądnego"
            )
            if block_expiry == "dnia sądnego":
                admins = self.bot.config.get_channel("admins")
                await admins.send(embed=discord.Embed(
                    colour=discord.Colour.random(seed=member.id),
                    description=(
                        f"Konto {signature} należące do "
                        f"{member.mention} jest zablokowane na zawsze. "
                        f"Użytkownik ma nadal dostęp do serwera."
                    )
                ).set_footer(
                    text="Wiadomość wygenerowana przez komendę !whois."
                ))
            block_reason = user_data.get("blockreason")
            blocked = f"Zablokowany przez {blocked_by} " \
                      + (f"(*{block_reason}*) " if block_reason else "") \
                      + f"do {block_expiry}"
        # Talk page and page with the user's contributions.
        talk_page = user_page.getUserTalkPage().full_url()
        contribs_page = pywikibot.Page(
            self.bot.site,
            f"Special:Contributions/{account.mw_username}"
        ).full_url()
        talk_and_contribs = f"[dyskusja]({talk_page}) • [wkład]({contribs_page})\n"

        embed = discord.Embed(
            colour=discord.Colour.random(seed=member.id),
            title=user_page.title(with_ns=False),
            description="\n".join(filter(
                truth,
                (talk_and_contribs, on_leave, registration, mention, contribs, groups, blocked)
            )),
            url=user_page.full_url(),
        )

        return embed

    # Background tasks
    @Cog.listener()
    async def on_ready(self):
        task = self.loop
        if task.is_running():
            task.restart()
        else:
            task.start()

    def filter_states(self, states: list, forbidden_members: list):
        """
        Prepare state list not contain states with forbidden members
        and not to process states of members who are absent from the guild.
        """
        states_ok = []
        for state in states:
            member = self.bot.main_guild.get_member(state.member_id)
            if member is not None and member not in forbidden_members:
                states_ok.append(state)
        return states_ok

    @loop(seconds=60, reconnect=True)
    async def loop(self):
        with self.bot.db.session() as sess:
            # A list of members to invoke bare self.initialize(sess, member) on.
            to_init = []

            # Filter members to be verified basing on the real guild members.
            for member in self.bot.main_guild.members:
                if member.roles != [self.bot.config.get_role("everyone")]:
                    continue
                state = sess.get(State, member.id)
                if state is None:
                    to_init.append(member)
                    continue
                account = sess.get(Account, member.id)
                if account is not None:
                    account_data = pywikibot.User(
                        self.bot.site, "User:" + account.mw_username
                    ).getprops(True)
                    await self.submit_member(sess, state, member, account, account_data)

            # Open a query for pending authentication states.
            pending = sess.query(State).filter_by(done=False)

            # Specify expired states.
            kick_states = pending.filter(State.auth_expires_at <= datetime.datetime.now()).all()

            # Compute time indicating soon expiry that determines whether to remind a member about
            # auto-kick or not.
            soon_expires = datetime.datetime.now() + self.time_left_to_remind

            # Specify states of members who are to be reminded.
            remind_states = pending.filter(
                State.auth_expires_at <= soon_expires,
                State.reminded.is_(False)
            ).all()

            # Prepare state list not to contain duplicates from to_init states and not to process
            # states of members who are absent from the guild.
            to_kick = self.filter_states(kick_states, to_init)

            # Do the same as above preventing reminding members and kicking them after that
            # immediately.
            to_remind = self.filter_states(remind_states, to_init + to_kick)

            await asyncio.gather(
                asyncio.gather(*(self.initialize(sess, member) for member in to_init)),
                asyncio.gather(*(self.kick_member_after_expiry(sess, state) for state in to_kick)),
                asyncio.gather(*(self.initialize(sess, self.bot.main_guild.get_member(
                    state.member_id), remind=True) for state in to_remind))
            )
