"""
Cogs used for the Nonsensopedia Discord Bot.
"""

from .auth import Auth
from .meetings import Meetings
from .link import Link
from .logger import Logger
from .reactionroles import ReactionRoles
from .gitinfo import GitInfo
