"""
Plugin that registers and implements "reaction roles".
"""

import traceback
from typing import Coroutine, Optional, Union

import discord

from ndb.bot.models import ReactionRole
from ndb.lib import Cog


class ReactionRoles(Cog):
    __name__ = "reaction_roles"

    async def find_reaction_role(
            self,
            sess,
            event: Union[
                discord.RawReactionActionEvent,
                discord.RawReactionClearEvent,
            ]
    ) -> Optional[ReactionRole]:
        guild = self.bot.get_guild(event.guild_id)
        member = event.member or await guild.fetch_member(event.user_id)
        reaction_role = sess.query(ReactionRole).filter(
            ReactionRole.member_id.in_([0, member.id]),
            ReactionRole.guild_id == guild.id,
            ReactionRole.message_id == event.message_id,
            ReactionRole.emoji_id == event.emoji.id,
            ReactionRole.channel_id == event.channel_id,
        ).one_or_none()
        return reaction_role

    @Cog.listener()
    async def on_raw_reaction_add(
            self, event: discord.RawReactionActionEvent
    ):
        """Handle addition of a raw reaction for "reaction roles"."""
        with self.bot.db.session() as sess:
            reaction_role = await self.find_reaction_role(sess, event)
            if reaction_role is None:
                return
            guild = self.bot.get_guild(reaction_role.guild_id)
            member = guild.get_member(reaction_role.member_id)
            await member.add_roles(guild.get_role(reaction_role.role_id))
            try:
                if reaction_role.callback is not None:
                    callback = eval(reaction_role.callback)
                    if callable(callback):
                        callback = callback()
                    if isinstance(callback, Coroutine):  # pylint: ignore=W1116
                        await callback
            except Exception:
                traceback.print_exc()
            finally:
                if reaction_role.temporary:
                    sess.delete(reaction_role)

    @Cog.listener()
    async def on_raw_reaction_remove(
            self, event: discord.RawReactionClearEvent
    ):
        """Handle deletion of a raw reaction for "reaction roles"."""
        with self.bot.db.session() as sess:
            reaction_role = await self.find_reaction_role(sess, event)
            if reaction_role is None:
                return
            guild = self.bot.get_guild(reaction_role.guild_id)
            member = guild.get_member(reaction_role.member_id)
            await member.remove_roles(guild.get_role(reaction_role.role_id))
