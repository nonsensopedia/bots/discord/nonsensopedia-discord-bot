"""Log bot events to custom streams."""

import logging
import sys
from contextlib import suppress
from operator import truth

import discord
from colorama import Fore

from ndb.bot.cogs.auth import cast_webhook_response
from ndb.bot.tools import class_logger
from ndb.lib import Cog


class _DummyFore:
    def __getattr__(self, item):
        return ''


dummy_fore = _DummyFore()
default_formatter = logging.Formatter(
    '%(asctime)s %(msg)s',
    f'{Fore.LIGHTBLACK_EX}:{Fore.RESET}'.join(
        f'{Fore.YELLOW}{t}{Fore.RESET}' for t in '%H:%M:%S'.split(':')
    )
)
default_streams = (sys.stdout,)


class Logger(Cog, class_logger("Logger", directory="../logs/")):
    __name__ = 'logger'
    streams = default_streams

    def __init__(self, *args, common_formatter=default_formatter, **kwargs):
        self.stream_handlers = [
            logging.StreamHandler(stream)
            for stream in self.streams
        ]
        for handler in self.stream_handlers:
            handler.setFormatter(common_formatter)
        self.__logger.handlers = self.stream_handlers
        super().__init__(*args, **kwargs)

    def format_message(
            self,
            message: discord.Message,
            time=False,
            guild=True,
            channel=True,
            colour=True,
            timefmt='%H:%M:%S'
    ):
        fore = Fore if colour else dummy_fore

        if message.webhook_id == self.bot.auth.oauth.data["webhook"]:
            with suppress(ValueError):
                response = cast_webhook_response(message.clean_content)
                member = self.bot.main_guild.get_member(response["discord_id"])
                return (
                    f"{fore.BLUE}({message.guild}){fore.RESET} "
                    f"AuthApp webhook dla {member}"
                )

        embeds = ' '.join(
            embed.description for embed in message.embeds
            if embed.description is not discord.Embed.Empty
        )
        files = ' '.join(
            ('Plik: ' + attachment.filename) for attachment in message.attachments
            if attachment.filename is not None
        )
        content = ' '.join(filter(truth, [message.clean_content, embeds, files]))
        time_string = message.created_at.strftime(timefmt)
        message_string = (time_string + ' ') if time else ''

        if isinstance(message.channel, discord.DMChannel):
            message_string += (
                (f'{fore.LIGHTBLACK_EX}(DM: {message.channel.recipient}) ' if channel else '')
                + f'{fore.LIGHTBLACK_EX}<{fore.RED}{message.author}'
                f'{fore.LIGHTBLACK_EX}>{fore.RESET} '
                f'{content}'
            )
        else:
            message_string += (
                (f'{fore.GREEN}({message.guild}) ' if guild else '')
                + (f'{fore.CYAN}[#{message.channel}] ' if channel else '')
                + f'{fore.LIGHTBLACK_EX}<{fore.RESET}{message.author}'
                f'{fore.LIGHTBLACK_EX}>{fore.RESET} '
                f'{content}'
            )
        return message_string

    @staticmethod
    def format_member_join(member, colour=True):
        fore = Fore if colour else dummy_fore
        return f'{fore.GREEN}({member.guild}){fore.RESET} {member} dołączył do serwera'

    @staticmethod
    def format_member_remove(member, colour=True):
        fore = Fore if colour else dummy_fore
        return f'{fore.GREEN}({member.guild}){fore.RESET} {member} wyszedł z serwera'

    def format_ready(self, colour=True):
        fore = Fore if colour else dummy_fore
        return f'{fore.GREEN}(!){fore.RESET} Bot {fore.CYAN}{self.bot.user}{fore.RESET} gotowy'

    @Cog.listener()
    async def on_ready(self):
        self.__log(self.format_ready())

    @Cog.listener()
    async def on_message(self, message):
        self.__log(self.format_message(message))

    @Cog.listener()
    async def on_member_join(self, member):
        self.__log(self.format_member_join(member))

    @Cog.listener()
    async def on_member_remove(self, member: discord.Member):
        self.__log(self.format_member_remove(member))
