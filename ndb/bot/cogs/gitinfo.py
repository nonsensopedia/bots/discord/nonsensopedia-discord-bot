import textwrap

import discord
from babel.dates import format_datetime
from discord.ext.commands import command
from git.repo import Repo

from ndb import repo_path
from ndb.bot.tools import has_roles
from ndb.lib import Cog


class GitInfo(Cog):
    repo = Repo(repo_path)

    @command("version", aliases=["wersja", "git"])
    @has_roles("backend_admin")
    async def version(self, ctx):
        embed = self.create_version_embed(colour=discord.Colour.random(seed=ctx.author.id))
        await ctx.reply(embed=embed)

    def create_version_embed(self, colour=None):
        commit = self.repo.head.commit
        commited_dt = commit.committed_datetime
        embed = discord.Embed(
            title=textwrap.shorten(commit.hexsha, width=8, placeholder=""),
            colour=colour,
            description="*" + discord.utils.escape_markdown(commit.summary) + "*",
        ).set_footer(text=format_datetime(commited_dt, locale="pl_PL"))
        return embed
