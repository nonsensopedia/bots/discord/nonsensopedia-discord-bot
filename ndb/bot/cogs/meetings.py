"""
Cog for organising meetings.
See also <https://nonsa.pl/wiki/Nonsensopedia:Spotkania>.
"""
import datetime
import io

import discord
import sqlalchemy
from discord.ext.commands import command, max_concurrency, BucketType

from ndb.bot.cogs.auth import TZINFO
from ndb.bot.models import Meeting
from ndb.bot.tools import has_roles
from ndb.lib import Cog

OVERWRITTEN_PERMISSIONS = 'add_reactions', 'send_messages', 'attach_files'
"""Channel permissions that are overwritten by the bot each time it opens or closes a meeting"""


class Meetings(Cog):
    @property
    def meetings_channel(self) -> discord.TextChannel:
        return self.bot.config.get_channel("meetings")

    @staticmethod
    def get_ongoing_meeting(sess):
        """Checks for an ongoing meeting."""
        return sess.query(Meeting).filter_by(closed=None).one_or_none()

    async def can_open(self, sess, can=True, cannot=False, ctx=None):
        """Checks if you can open a meeting."""
        now = datetime.datetime.utcnow()
        if self.get_ongoing_meeting(sess) is not None:
            if ctx is not None:
                await ctx.reply(embed=discord.Embed(
                    colour=discord.Colour.random(seed=ctx.author.id),
                    description=f':no_entry: Spotkanie {self.date_xform(now)} jest już otwarte.'
                ))
            return cannot
        present = sess.query(Meeting).filter(
            sqlalchemy.extract('year', Meeting.opened) == now.year,
            sqlalchemy.extract('month', Meeting.opened) == now.month,
            sqlalchemy.extract('day', Meeting.opened) == now.day,
        ).one_or_none()
        if present is not None:
            sess.delete(present)
            sess.commit()
        return can

    async def can_close(self, sess, cannot=None, ctx=None):
        """
        Checks if you can close a meeting. Returns the meeting object if so. Otherwise,
        returns the *cannot* value.
        """
        meeting = self.get_ongoing_meeting(sess)
        if meeting is None:
            await ctx.reply(embed=discord.Embed(
                colour=discord.Colour.random(seed=ctx.author.id),
                description=':no_entry: Spotkanie nie rozpoczęło się.'
            ))
            return cannot
        return meeting

    @staticmethod
    def date_xform(meeting_dt, time=''):
        """
        Changes the date's timezone onto the bot's timezone (see ndb/bot/cogs/auth.py) and,
        by default, returns the meeting date in the %Y-%m-%d format (ISO until the 10th char).
        """
        return TZINFO.fromutc(
            meeting_dt.replace(tzinfo=TZINFO)
        ).strftime('%Y-%m-%d' + ((' ' + time) if time else ''))

    @max_concurrency(1, BucketType.user)
    @command(name='otwórzspotkanie')
    @has_roles('admin')
    async def open_meeting(self, ctx):
        """Open a meeting."""
        with self.bot.db.session() as sess:
            if await self.can_open(sess, ctx=ctx):
                await self._open_meeting_impl(sess, ctx)

    async def _open_meeting_impl(self, sess, ctx):
        now = datetime.datetime.utcnow()
        embed = discord.Embed(
            colour=discord.Colour.random(seed=ctx.author.id),
            description=f"Spotkanie {self.date_xform(now)} otwarte!"
        ).set_footer(text=f"Otwarte przez: {ctx.author.name}.")
        meeting = Meeting(
            id=int(now.timestamp()),
            guild_id=ctx.guild.id,
            opened_by=ctx.author.id,
            opened=now
        )
        sess.add(meeting)
        sess.commit()
        message = await self.meetings_channel.send(embed=embed)
        await message.pin(reason='Rozpoczęto spotkanie')
        await self.bot.config.get_channel('project').send(embed=embed)
        overwrite = self.meetings_channel.overwrites_for(self.bot.auth.authenticated_role)
        overwrite.update(**dict.fromkeys(OVERWRITTEN_PERMISSIONS, True))
        await self.meetings_channel.set_permissions(
            self.bot.auth.authenticated_role,
            overwrite=overwrite,
            reason='Rozpoczęto spotkanie'
        )

    @command(name='zamknijspotkanie')
    @has_roles('admin')
    async def close_meeting(self, ctx):
        """Close a meeting."""
        with self.bot.db.session() as sess:
            meeting = await self.can_close(sess, ctx=ctx)
            if meeting is not None:
                await self._close_meeting_impl(sess, ctx, meeting)

    async def _close_meeting_impl(self, sess, ctx, meeting):
        now = datetime.datetime.utcnow()
        embed = discord.Embed(
            colour=discord.Colour.random(seed=ctx.author.id),
            description=f"Spotkanie {self.date_xform(meeting.opened)} zakończyło się."
        ).set_footer(text=f"Zamknięte przez: {ctx.author.name}.")
        meeting.closed_by = ctx.author.id
        meeting.closed = now
        sess.commit()
        await self.meetings_channel.send(embed=embed)
        await self.bot.config.get_channel('project').send(embed=embed)
        file = await self.fetch_log_file(meeting)
        await self.meetings_channel.send(file=file)
        file.reset()
        await self.bot.config.get_channel('project').send(file=file)
        overwrite = self.meetings_channel.overwrites_for(self.bot.auth.authenticated_role)
        overwrite.update(**dict.fromkeys(OVERWRITTEN_PERMISSIONS, False))
        await self.meetings_channel.set_permissions(
            self.bot.auth.authenticated_role,
            overwrite=overwrite,
            reason='Zakończono spotkanie'
        )

    async def fetch_log_file(self, meeting) -> discord.File:
        """Fetches the log file of a meeting."""
        closed, opened = (
            # We extend the meeting time frame to catch the message of the person opening
            # the meeting and the bot's message confirming the closure of the meeting
            # (…may fail in case of a mighty lag).
            meeting.closed + datetime.timedelta(seconds=0.5),
            meeting.opened - datetime.timedelta(seconds=0.5)
        )
        lines = await self.meetings_channel.history(
            limit=None, after=opened, before=closed
        ).flatten()
        fp = io.StringIO(
            '\n'.join(
                self.bot.logger.format_message(
                    line,
                    time=True,
                    guild=False,
                    channel=False,
                    colour=False
                )
                for line in lines
            )
            or '\n'.join((
                'https://nonsa.pl/wiki/Plik:Strohballen.gif',
                '------------------------------------------',
                'Ups! Wygląda na to, że tu nic nie ma.',
                'Wiadomości z tego spotkania z jakiegoś powodu nie są dostępne.',
                'Skontaktuj się z którymś administratorem.',
            ))
        )
        return discord.File(fp, self.date_xform(meeting.opened) + '.log')
