from ndb.bot.settings import settings
from ndb.lib import Bot

Bot = type("Bot", (Bot,), {})
Bot.configure(**settings)  # noqa
