"""
Miscellaneous utilities.
"""

import functools
import inspect
import logging
import os
import re
import sys
import traceback
from typing import List, Union

import discord
import pywikibot
from discord.abc import Messageable
from discord.ext.commands import Command, CommandOnCooldown, Cooldown, CooldownMapping
from pywikibot.backports import removeprefix


def logging_class(cls=None, **kwargs):
    """
    Decorator for a class for automatic class_logger() inheritance.

    Examples
    --------
    @logging_class
    class Spam:
        def eggs(self):
            self.__log("Spam.eggs() works marvellous")

    """
    if cls is None:
        return functools.partial(logging_class, **kwargs)
    kwargs = {
        "name": cls.__name__,
        "global_ctx": inspect.stack()[1][0].f_globals,
        **kwargs
    }
    cls.__bases__ += (class_logger(**kwargs),)
    return cls


def class_logger(
        name,
        level=None,
        global_ctx=None,
        more_handlers=(),
        debug=False,
        **kwargs
):
    """
    Create a logging mixin (and level if not provided) using caller's globals
    for the main class of a module.

    Examples
    --------
    class Spam(class_logger("Spam")):
        def eggs(self):
            self.__log("Spam.eggs() works perfect")

    """
    ctx = global_ctx or inspect.stack()[1][0].f_globals
    if not level:
        level = logging.DEBUG + int(abs(hash(name) * .001 // 1))
    file_handler = _manage_file_handler(level, ctx, name, **kwargs)
    handlers = [file_handler, *more_handlers]
    return _class_logger_factory(name, level, handlers, debug)


def _manage_file_handler(
        level,
        global_ctx,
        name=None,
        directory="logs",
        fmt="%(asctime)s [%(levelname)s] %(message)s",
        date_fmt="%Y-%m-%d %H:%M:%S"
):
    """Create logging.FileHandler and logging.Formatter for a module."""
    file = global_ctx["__file__"]
    name = file_name = name or global_ctx["__name__"]
    logging.addLevelName(level, name)
    rel_path = f'{directory}/{file_name}.log'
    abs_path = os.path.join(os.path.dirname(file), rel_path)
    if not os.path.isdir(os.path.dirname(abs_path)):
        os.mkdir(os.path.dirname(abs_path))
    if not os.path.isfile(abs_path):
        with open(abs_path, 'w+', encoding="utf-8") as log_file:
            log_file.write('-> {} logs\n'.format(file_name))
            log_file.write('-> from {}\n\n'.format(file))
    formatter = logging.Formatter(fmt, datefmt=date_fmt)
    file_handler = logging.FileHandler(abs_path, encoding="UTF8")
    file_handler.setFormatter(formatter)
    return file_handler


def _class_logger_factory(
        name: str,
        level: Union[int, str],
        handlers: List[logging.Handler],
        debug: bool = False
):
    """Factory function that produces a class logger."""

    class ClassLogger:
        logger = logging.getLogger(name)
        logger.setLevel(level)

        if not debug:
            logging.disable(logging.DEBUG)

        logger.handlers = handlers

        @property
        def __logger(self, logger=logger):
            return logger

        def __log(self, msg, *args, **kwargs):
            self.__logger.log(level, msg, *args, **kwargs)

        log = __log

    type.__setattr__(ClassLogger, f"_{name}__logger", ClassLogger.logger)
    type.__setattr__(ClassLogger, f"_{name}__log", ClassLogger().log)
    return ClassLogger


def from_hint(user_hint, ctx, guild=None):
    if isinstance(user_hint, tuple):
        user_hint = " ".join(user_hint)
    mention_regex = re.compile(r"<?@!?(?P<discord_id>(\d{18}|.+))>?")
    mention = mention_regex.search(user_hint)
    if guild is None:
        guild = ctx.guild
    if not user_hint:
        target = guild.get_member(ctx.message.author.id)
    elif len(user_hint) == 18 and user_hint.isdigit():
        target = guild.get_member(int(user_hint.strip()))
    elif mention:
        discord_id = mention.group("discord_id")
        target = from_hint(discord_id, ctx, guild)
        if target is None:
            target = guild.get_member_named(discord_id)
    else:
        target = guild.get_member_named(user_hint.strip())
    return target


def inflect_timedelta(timedelta):
    timedelta = timedelta \
        .replace("godzina", "godzinę", 1) \
        .replace("sekunda", "sekundę", 1) \
        .replace("minuta", "minutę", 1)
    return removeprefix(timedelta, "1 ")


async def ping(member, channel: Messageable):
    # NOTE: Usage ommit of delete_after= is intended.
    message = await channel.send(f"||{member.mention}||")
    await message.delete()


def user_signature(site, username):
    user_page = pywikibot.User(site, "User:" + username)
    contribs = pywikibot.Page(
        user_page.site, "Special:Contributions/" + user_page.username
    )
    discussion = user_page.getUserTalkPage()
    markdown = (
        f"[{user_page.username}]({user_page.full_url()}) "
        f"([dyskusja]({discussion.full_url()}) • "
        f"[wkład]({contribs.full_url()}))"
    )
    return markdown


def has_roles(name, allow_higher_roles=True, allow_webhook=0):
    # NOTE:
    #   This isn't a discord.ext.commands.check because the check has to take only one parameter
    #   representing the command context.
    def wrapper(func):
        @functools.wraps(func)
        async def callee(self, ctx, *args, **kwargs):
            if isinstance(ctx.author, discord.User):
                if self.bot.main_guild not in ctx.author.mutual_guilds:
                    return await func(self, ctx, *args, **kwargs)
                member = self.bot.main_guild.get_member(ctx.author.id)
            else:
                member = ctx.author

            allow = False
            is_ignorable_webhook = ctx.message.webhook_id == allow_webhook

            if not is_ignorable_webhook:
                expected = self.bot.config.get_role(name).position
                positions = [role.position for role in member.roles]

                if member.id == self.bot.main_guild.owner_id:
                    # Always allow guild owner to perform any command.
                    allow = True
                for position in positions:
                    if allow:
                        break
                    if (allow_higher_roles and position > expected) or position == expected:
                        allow = True

                if not allow:
                    return await no_access_fallback(member, ctx)

            return await func(self, ctx, *args, **kwargs)

        async def no_access_fallback(member, ctx):
            await ctx.reply(embed=discord.Embed(
                colour=discord.Colour.random(seed=member.id),
                title="Błąd!",
                description=(
                    ":no_entry: "
                    "Nie posiadasz uprawnień do wywołania tej komendy."
                ))
            )

        return callee

    return wrapper


class AdaptiveCooldownMapping(CooldownMapping):
    def __init__(self, original, predicate=None):
        super().__init__(original)
        self.predicate = predicate

    class Helper:
        @staticmethod
        def update_rate_limit(*_args, **_kwds):
            return 0

    def get_bucket(self, message, current=None):
        bucket = super().get_bucket(message, current)
        if self.predicate(message):
            return bucket
        return self.Helper

    def copy(self):
        ret = type(self)(self._cooldown)
        ret._cache = self._cache.copy()
        ret.predicate = self.predicate
        return ret


async def on_cooldown_error(*_args, **_kwargs):
    exc = sys.exc_info()[1]
    if isinstance(exc, CommandOnCooldown):
        print(f'Cooldown: command locked for {exc.retry_after} seconds', file=sys.stderr)  # noqa
    else:
        traceback.print_exc()


def skip_cooldown_if_admin(message):
    if isinstance(message.author, discord.Member):
        return message.author.guild_permissions != discord.Permissions.all()
    return True


def cooldown(rate, per, typ, predicate=None, err_handler=on_cooldown_error):
    def inner(func):
        if isinstance(func, Command):
            func._buckets = AdaptiveCooldownMapping(Cooldown(rate, per, typ), predicate)
            func.error(err_handler)
        else:
            func.__commands_cooldown__ = Cooldown(rate, per, type)
        return func

    return inner
