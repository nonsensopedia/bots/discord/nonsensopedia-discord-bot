"""Nonsensopedia Discord Bot configuration file."""

from os import path
from pathlib import Path

import pywikibot

from ndb.bot.cogs import Auth, GitInfo, Link, Logger, Meetings, ReactionRoles
from ndb.bot.family import Nonsensopedia
from ndb.lib import BotDatabase

config_path = Path(path.dirname(__file__)).parent.joinpath("config.ini")
settings = dict(
    cog_classes=(Auth, GitInfo, Link, Logger, Meetings, ReactionRoles),
    db_class=BotDatabase,
    config_string=open(config_path, "r").read(),
    site=pywikibot.Site(code="pl", fam=Nonsensopedia()),
)
