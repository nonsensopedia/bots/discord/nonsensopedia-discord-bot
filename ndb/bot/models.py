"""
SQLAlchemy database models used by Nonsensopedia Discord Bot.
"""

from dataclasses import dataclass
from typing import Any, Union

from sqlalchemy import (
    Boolean,
    Integer,
    Column,
    DateTime,
    String,
    UnicodeText,
)

from ndb.lib import DeclarativeGuildBase as Model

__all__ = ("Account", "State", "Meeting", "ReactionRole",)

Field = Union[Any, Column]  # type annotations determine dataclass fields


@dataclass
class Account(Model):
    """Represents a Nonsensopedia account."""

    __tablename__ = "accounts"
    mw_userid: Field = Column(Integer)
    mw_username: Field = Column(UnicodeText)
    mw_gender: Field = Column(String)
    member_id: Field = Column(Integer, primary_key=True)
    guild_id: Field = Column(Integer)


@dataclass
class Meeting(Model):
    """Represents a "meeting" (extra guild event)."""

    __tablename__ = "meetings"
    id: Field = Column(Integer, primary_key=True)
    guild_id: Field = Column(Integer)
    opened_by: Field = Column(Integer)
    closed_by: Field = Column(Integer, nullable=True)
    opened: Field = Column(DateTime)
    closed: Field = Column(DateTime, nullable=True)


@dataclass
class ReactionRole(Model):
    """Represents a "reaction role" (extra guild event)."""

    __tablename__ = "reaction_roles"
    id: Field = Column(Integer, primary_key=True)
    member_id: Field = Column(Integer)
    message_id: Field = Column(Integer)
    guild_id: Field = Column(Integer)
    channel_id: Field = Column(Integer)
    emoji_id: Field = Column(Integer)
    role_id: Field = Column(Integer)
    temporary: Field = Column(Boolean)
    callback: Field = Column(String)


@dataclass
class State(Model):
    """Represents a user's authentication state."""

    __tablename__ = "authentication_states"
    guild_id: Field = Column(Integer)
    member_id: Field = Column(Integer, primary_key=True)
    url_expires_at: Field = Column(DateTime)
    auth_expires_at: Field = Column(DateTime)
    redirect_url: Field = Column(UnicodeText)
    request_token: Field = Column(String)
    access_token: Field = Column(String, nullable=True)
    reminded: Field = Column(Boolean, default=False)
    done: Field = Column(Boolean, default=False)
