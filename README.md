# Nonsensopedia Discord Bot
This is an application for a Discord bot for Nonsensopedia 
that manages the Nonsensopedia Discord server.

## As for now, it:
- authenticates new members via Nonsensopedia using 
  [MediaWiki OAuth extension](https://www.mediawiki.org/wiki/Extension:OAuth),
- replies with Markdown links to each message with an 
  `[[internal link]]` or a `{{template reference}}`,
- supports reaction roles, similar to those described 
  [there](https://docs.channelbot.xyz/guides/reaction-roles).

## In the future, it will:
- listen to 
  [recent changes](https://www.mediawiki.org/wiki/Help:Recent_changes) and
  broadcast them through a special channel,
- provide tools for automated scheduling 
  [meetings (PL)](https://nonsa.pl/wiki/Nonsensopedia:Spotkania).

# Setup
First, clone this repository using `git`.
Change the current directory to the one you will have this repository in.
Run the following command:

``git clone https://gitlab.com/nonsensopedia/bots/discord/nonsensopedia-discord-bot.git``

After cloning the repository, 
you need to install the appropriate dependencies.

``pip install -r requirements.txt``

Next, you will need to rename `mwoauth.json.example` file 
(in `ndb` directory) to `mwoauth.json`, 
changing the "changeit" fields with the appropriate data for 
MediaWiki OAuth transactions. You can also place the file elsewhere and specify its location with the `MWOAUTH_FILE` environment variable.

You can create your own setup function in 
`ndb/local.py` that will be run every time you start the application.
After modifying, please don't commit it to this git repository!

`ndb/config.ini` file contains Discord-specific configuration, 
which can be changed locally, depending on the guild 
that the bot will work with. In this repository, it is updated 
according to the official Nonsensopedia Discord server.

# Launching the application
Usage:

``python -m ndb [--token DISCORD_TOKEN]``

The `--token` argument refers to the special token of the bot you run this application on 
(see also [Discord OAuth2 documentation](https://discord.com/developers/docs/topics/oauth2)).
It can be either provided through the command-line, or through the local environment variable `DISCORD_TOKEN`. Alternatively, the token can be provided with a file, specified by the `DISCORD_TOKEN_FILE` variable.

When launched, the application creates a `.db` file in the top-level directory
(super directory `ndb/`). It will store data such as
authenticated member accounts, meetings, reaction roles, etc.
**If the `.db` file is damaged or lost, all that data will be lost,
therefore, it is strongly recommended making regular backups**.

## Running in Docker

You should take care to persist the database somehow. You can use the `DB_CONN_STRING` environment variable to change the connection string to anything you want, either an SQLite file or an external DB. Please refer to [sqlalchemy documentation](https://docs.sqlalchemy.org/en/14/core/engines.html) for more info.
